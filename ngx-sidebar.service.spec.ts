import { TestBed, inject } from '@angular/core/testing';

import { NgxSidebarService } from './sidebar.service';

describe('NgxSidebarService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NgxSidebarService]
    });
  });

  it('should be created', inject([NgxSidebarService], (service: NgxSidebarService) => {
    expect(service).toBeTruthy();
  }));
});
