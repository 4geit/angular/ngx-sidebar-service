import { InjectionToken } from '@angular/core';

export const ITEMS = new InjectionToken< Array<string> >('items');
export const BASE_PATH = new InjectionToken<string>('basePath');
