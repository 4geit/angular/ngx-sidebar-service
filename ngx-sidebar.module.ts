import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxSidebarService } from './ngx-sidebar.service';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    NgxSidebarService
  ],
  exports: [
  ]
})
export class NgxSidebarModule { }
