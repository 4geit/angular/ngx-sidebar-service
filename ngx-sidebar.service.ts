import { Inject, Injectable, Optional } from '@angular/core';

import { ITEMS, BASE_PATH } from './variables';

@Injectable()
export class NgxSidebarService {

  items = [];
  basePath = '/category';

  constructor(
    @Optional()@Inject(ITEMS) items: Array<string>,
    @Optional()@Inject(BASE_PATH) basePath: string,
  ) {
    if (items) { this.items = items; }
    if (basePath) { this.basePath = basePath; }
  }

  getItem(slug) {
    const results = this.items.filter(item => item.slug === slug);
    if (results.length) {
      return results[0];
    }
  }

}
